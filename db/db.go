package db

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/controllercubiomes/util"
)

type DBCred struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBCred) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) GetVerifiedSeeds(offset, limit string) (seeds []util.Seed, err error) {
	rows, err := db.Conn.Query("SELECT "+seedData+" FROM seeds WHERE verified = TRUE LIMIT $1 OFFSET $2;", limit, offset)
	if err != nil {
		log.Println(err)
	}
	seeds, err = rowsToSeeds(rows)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) GetNotVerifiedSeeds(offset, limit string) (seeds []util.Seed, err error) {
	rows, err := db.Conn.Query("SELECT "+seedData+" FROM seeds WHERE verified = FALSE LIMIT $1 OFFSET $2;", limit, offset)
	if err != nil {
		return
	}
	seeds, err = rowsToSeeds(rows)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) GetSeed(seedID int64) (seed util.Seed, err error) {
	row := db.Conn.QueryRow("SELECT "+seedData+" FROM seeds WHERE seed = $1;", seedID)
	if err != nil {
		return
	}
	seed, err = rowToSeed(row)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) GetSPS() (count int, err error) {
	row := db.Conn.QueryRow("SELECT count(*) FROM chunks WHERE (startTime > now() - interval '13 hours' AND seedsfound != null) OR (verStartTime > now() - interval '13 hours' AND verified != false);")
	if err != nil {
		return
	}
	err = row.Scan(&count)
	if err != nil {
		log.Println(err)
	}
	count = count * (1 << 30) / 13 / 60 / 60
	return
}

func (db DB) GetTotalScanned() (count int, err error) {
	row := db.Conn.QueryRow("SELECT count(*) FROM chunks WHERE verified = TRUE;")
	if err != nil {
		return
	}
	err = row.Scan(&count)
	if err != nil {
		log.Println(err)
	}
	count = count * (1 << 30)
	return
}

func (db DB) GetTotalViable() (count int, err error) {
	row := db.Conn.QueryRow("SELECT count(*) FROM seeds WHERE verified = TRUE;")
	if err != nil {
		return
	}
	err = row.Scan(&count)
	if err != nil {
		log.Println(err)
	}
	return
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func (db DB) RegisterSystemKey() (key string, err error) {
	key = RandStringRunes(30)
	result, err := db.Conn.Exec("INSERT INTO searchers (key) VALUES ($1);", key)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}
